import { html,css,LitElement} from 'lit-element';

export class MyEmployes extends LitElement{
    static get properties(){
        return{
            employees:{type:Array, reflect:true}, 
            filterEmploye:{type:Array},
            valorBusqueda:{type:String},
            

        }
    }
    static get styles(){
        return css`
            body{
                background: whitesmoke;
                margin: 0;
                width: 100%;
                height: 100%;
            }
            #encabezado{
                background:#4cb1e4;
                height: 40px;
                color: white;
                font-weight: none;
            }
            #header{
                padding-top:7px;
                padding-left: 30px;
                background: #8a8d8d;
                height: 30px;
                color: black;
            }
           
            #thead{
                background:rgb(147, 182, 187);
                text-align: left;
            }
            caption, td, th {
                padding: 0.3em;
             }
             th, td {
                border-bottom: 1px solid #9db3ee;
                width: 25%;
             }
             .button {
                display: inline-block;
                padding: 10x 10px;
                font-size: 14px;
                cursor: pointer;
                text-align: center;	
                text-decoration: none;
                outline: none;
                color: #0a0a0a;
                background-color: #5ce9e9;
                border: none;
                border-radius: 15px;
                box-shadow: 0 3px #999;
            }

            .button:hover {background-color: #4f6650}

            .button:active {
                background-color: #3e8e41;
                box-shadow: 0 5px #666;
                transform: translateY(4px);
            }        
             
        `;
    }

    constructor(){
        super();
        this.employees = [];
        this.filterEmploye = [];
        this.valorBusqueda = '';
    }
    firstUpdated(){
        fetch('http://dummy.restapiexample.com/api/v1/employees')
        .then(response => response.json())
        .then(data => {
        
            this.employees = data.data;
            // data.data.map(employe=>{
            //     this.employees.push(employe);
            //     return employe;
            // });
        });
    }
    
    
    listarAsc(){
        this.employees=this.employees.sort((a,b)=> a.employee_name.localeCompare(b.employee_name));
        console.log(this.employees);
        this.requestUpdate();
    }
    listarDesc(){
        this.employees=this.employees.sort((b,a)=> a.employee_name.localeCompare(b.employee_name));
        console.log(this.employees);
        this.requestUpdate();
    }
    inputEmployees(e){

        this.valorBusqueda = e.target.value;
        // this.filterEmploye = this.employees.filter(filtro=> filtro.employee_name.localeCompare(this.valorBusqueda));
        // console.log(this.filterEmploye);
      
    }

    filterEmployees(){
        let parseo = parseInt(this.valorBusqueda);
        if (this.valorBusqueda != '') {
            this.shadowRoot.querySelectorAll('input')[0].value=""; //Limpiando el input
            this.employees = this.employees.filter(i => i.employee_name === this.valorBusqueda || i.id === parseo );
            console.log(this.employees);
            
            
            if (this.employees.length != 0) {
<<<<<<< HEAD
                this.shadowRoot.querySelectorAll('input')[0].value=""; //Limpiando el input
=======
>>>>>>> 93e1211741f18641a7df3f2b912094bf6751a16f
                this.requestUpdate();
                
            }else{
                alert('Usuario no encontrado, recargando tabla!');
<<<<<<< HEAD
                this.shadowRoot.querySelectorAll('input')[0].value=""; //Limpiando el input
=======
>>>>>>> 93e1211741f18641a7df3f2b912094bf6751a16f
                this.firstUpdated();
            }
        }
        else{
            alert('Introduce un campo!!');
        }
<<<<<<< HEAD
        
=======
>>>>>>> 93e1211741f18641a7df3f2b912094bf6751a16f
      
    }
  
    render(){
        console.log('se actualizó el render');
        return html `
            <h1 id="encabezado">Lista de empleados</h1>
            
            <section id="header">
                <input type="text" placeholder="Escribe nombre o ID" @input="${this.inputEmployees}">
                <button @click="${this.filterEmployees}" class="button">Buscar</button>
                <button @click="${this.listarAsc}" class="button">OrdenarAsc</button>
                <button @click="${this.listarDesc}" class="button">OrdenarDesc</button>
                <button @click="${this.firstUpdated}" class="button">MostrarTablaIncial</button>
            </section>
            <table>
                <thead id="thead">
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Dinero</th>
                        <th>Age</th>
                    </tr>
                </thead>
                <tbody>
                    ${this.employees.map(employe=>{
                    return html`
                     <tr>
                        <td>${employe.id}</td>
                        <td>${employe.employee_name}</td>
                        <td>${employe.employee_salary}</td>
                        <td>${employe.employee_age}</td>
                    </tr>
                    
                    `;
                    })}
               </tbody>
            </table>
            
        
        
        `;
    }
}

customElements.define('my-employes',MyEmployes);
