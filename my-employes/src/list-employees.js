import {html, LitElement} from 'lit-element'
import './my-employes.js';

export class ListEmployees extends LitElement{

    //Este componente solo fue creado para entender el funcionamiento del doble binding en LITELEMENT, NO TIENE FUNCIONALIDAD EN EL INDEX
    static get properties(){
        return{
            idEmploye:{type:Number},
            nameEmploye:{type:String},
            salaryEmploye:{type:Number},
            ageEmploye:{type:Number},
            listEmploye:{type:Array},


        }
    }
   
    constructor(){
        super();
        this.idEmploye = null;
        this.nameEmploye = '';
        this.salaryEmploye = null;
        this.ageEmploye = null;
        this.listEmploye = [];
    }
    dataHandler(e){
        // this.idEmploye = e.detail.id;
        console.info(e.target);
        this.listEmploye = e.target.employees;
        console.info(this.listEmploye);
        // this.nameEmploye = e.target.employees.employee_name;
        // this.salaryEmploye = e.detail.employee_salary;
        // this.ageEmploye = e.detail.employee_age;
        
    }
    render(){
        return html`
            
            <my-employes
                @my-employe="${this.dataHandler}"
            
            ></my-employes>

            <table>
                ${this.listEmploye.map(employe=>{
                    return html`
                     <tr>
                        <td>${employe.id}</td>
                        <td>${employe.employee_name}</td>
                        <td>${employe.employee_salary}</td>
                        <td>${employe.employee_age}</td>
                    </tr>
                    
                    `;
                })}
               
            </table>

          
          
        
        `;
    }
}

customElements.define('list-employees',ListEmployees);