import {html, css, LitElement} from 'lit-element'

export class ListItem extends LitElement{
    static get properties(){
        return{
            arregloItem:{type:Array},
            prueba:{type:String},
            checkDelete:{type:Array},
            valueList:{type:String},

        }
        

    }
    constructor(){
        super();
        this.arregloItem = [];
        this.valueList = '';
        this.checkDelete = [];

    }

    addItemList(){

        if (this.valueList === '') {
            alert('Introduce un Item en tu WishList')
        }else{
            this.arregloItem.push(this.valueList);
            this.shadowRoot.querySelectorAll('input')[0].value="";
            this.valueList='';
            this.requestUpdate();        
        }
        }
        

    inputList(e){
        this.valueList = e.target.value;
    }

    refreshList(){
        const checkSelect = this.shadowRoot.querySelectorAll('.nombre');
        
        for (let i = 0; i < checkSelect.length; i++) {
            if (checkSelect[i].checked == true) {
                this.checkDelete.push(i);
            }    
                 
        }
        for (let j = this.checkDelete.length -1; j >= 0; j--) {
            this.arregloItem.splice(this.checkDelete[j],1);
            
        }
        this.requestUpdate();
        console.log(this.arregloItem);


    }

    render(){
        return html `
        
        <form>
            <h1>My wishlist</h1>

                <fieldset>
                    <legend>New wish</legend>
                    <br>
                    
                        <input type="text" placeHolder="Escribe tu nuevo wishlist" @input="${this.inputList}">
                        
                        <input type="button"  @click="${this.addItemList}" value="Add">
                    
                </fieldset>
                ${this.arregloItem.map(item =>{
                    return html `
                         <ul>
                            <li>
                                <input type="checkbox" name="micheckbox" class="nombre"   >
                                ${item}

                            </li>
                         </ul>
                    
                    `;
                })}
               

            <input type="button" @click="${this.refreshList}" value="Archive done">

            </form>
        `;
    }
}
customElements.define('list-item', ListItem);